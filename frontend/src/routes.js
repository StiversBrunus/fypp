import React from 'react'
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import FormCadastroFoto from '../src/componentes/cadastro/fotografo/formCadastroFoto'
// import FormCadastroCliente from '../src/componentes/cadastro/cliente/formCadastroCliente'

import LoginCliente from './componentes/login/cliente/ContainerLoginCliente'
import LoginFotografo from './componentes/login/fotografo/ContainerLoginFotografo'

import RotasCadastroFotografo from './componentes/cadastro/fotografo/CadastroFotografo'
import RotasCadastroCliente from './componentes/cadastro/cliente/CadastroCliente'
// import FotografoCategorias from '../src/componentes/cadastro/fotografo/CadastroCategorias'
import Index from '../src/componentes/home/Index'

import TrabalhosFotogrado from './componentes/areaFotografo/trabalhos/Trabalhos'
import PortfolioFotogrado from './componentes/areaFotografo/portfolio/Portfolio'

import CategoriaCliente from '../src/componentes/areaCliente/categoria/ContainerCategoria'
import FotografosCliente from '../src/componentes/areaCliente/fotografo/listargemFotografos/ContainerFotografos'
import PerfilFotografoCliente from '../src/componentes/areaCliente/fotografo/perfilFotografo/ContainerPerfilFotografo'
import SelecaoDataHora from './componentes/areaCliente/categoria/selecaoData/FiltroDataHora'

const Routes = () => (
    <BrowserRouter>
        <Switch>
            {/*ROTAS BASICAS*/}
            <Route exact path='/' component={Index}/>
            <Route exact path='/fotografo/cadastro' component={FormCadastroFoto}/>
            <Route exact path='/cliente/cadastro' component={RotasCadastroCliente}/>
            <Route exact path='/fotografo/cadastroTeste' component={RotasCadastroFotografo}/>

            {/* <Route exact path='/fotografo/cadastro/categorias' component={FotografoCategorias}/> */}
            
            <Route exact path='/fotografo/login' component={LoginFotografo}/>
            <Route exact path='/cliente/login' component={LoginCliente}/>

            {/*ROTAS CMS FOTOGRAFO*/}
            <Route exact path='/fotografo/trabalhos' component={TrabalhosFotogrado}/>
            <Route exact path='/fotografo/portfolio' component={PortfolioFotogrado}/>

            {/*ROTAS CMS CLIENTE*/}
            <Route exact path='/cliente/categorias' component={CategoriaCliente}/>
            <Route exact path='/cliente/fotografos' component={FotografosCliente}/>
            <Route exact path='/cliente/fotografos/:idFotografo' component={PerfilFotografoCliente}/>
            <Route exact path='/cliente/categorias/:idCategoria/data' component={SelecaoDataHora} />

        </Switch>
    </BrowserRouter>
)

export default Routes