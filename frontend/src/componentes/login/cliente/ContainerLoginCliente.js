import React, {Component} from 'react'
import FormLoginCliente from './FormLoginCliente'
import imgClienteLogin from './imagens/cliente.png'
import VoltarHome from '../VoltarHome'

class ContainerLoginCliente extends Component {
    render() {
        return (
            <div>
                <VoltarHome/>            
                <div style={{marginTop:'1%'}}>
                    <div className="container center ">
                        <div className="row" id='linha_img_cadastro' style={{ backgroundColor: '#641682' }}>
                            <div className="col s6" id="coluna_imagem" style={{ paddingTop: '5em', marginBottom: '5em' }}>
                                <h4 style={{ marginBottom: '1em' }}>
                                    Faça login na sua<br></br>
                                    conta agora
                                </h4>
                                <img src={imgClienteLogin} alt="" style={{ marginBottom: '2em' }}></img>
                                <a href="">
                                    <h3>Ainda não possui uma conta?<br></br>
                                    clique aqui para fazer cadastro</h3>
                                </a>
                            </div>
                            <FormLoginCliente />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ContainerLoginCliente;
