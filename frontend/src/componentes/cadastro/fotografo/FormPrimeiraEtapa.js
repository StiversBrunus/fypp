import React, { Component } from 'react';

export class FormTerceiraEtapa extends Component {

	proxima = e => {
		e.preventDefault();
		this.props.proximaEtapa();
	};

	render() {
		const { values, handleChange } = this.props;
		return (
			<div id="coluna_dados_cadastro" className="col s6 center" style={{ paddingTop: '1em', backgroundColor: '#ffffff', borderTopRightRadius: '0.5em', borderBottomRightRadius: '0.5em' }}>
				<h4 style={{ marginBottom: '1em' }}>
					WOW! Nós também amamos<br></br>
                    fotografia! Conte-nos um<br></br>
                    pouquinho sobre você.
                </h4>

				<div className="row">
					<form className="col s12">
						<div className="row">
							<div className="input-field col s12">
								<input id="nome" type="text" onChange={handleChange('nome')} name="nome" type="text" className="validate"></input>
								<label htmlFor="nome">Nome:</label>
							</div>
						</div>
						<div className="row">
							<div className="input-field col s12">
								<input id="cep" type="text" onChange={handleChange('cep')} className="validate"></input>
								<label htmlFor="cep">CEP:</label>
							</div>
						</div>
						<div className="row">
							<div className="input-field col s12">
								<input id="telefone" type="text" onChange={handleChange('telefone')} className="validate"></input>
								<label htmlFor="telefone">Telefone:</label>
							</div>
						</div>
						<button style={{ marginBottom: '1.9em', marginRight: '1.5em' }} onClick={this.proxima} className="waves-effect waves-light btn right">
							<i className="material-icons right">
								arrow_forward
                        	</i>Próximo
                  		</button>
					</form>
				</div>
			</div>
		);
	}
}

export default FormTerceiraEtapa;