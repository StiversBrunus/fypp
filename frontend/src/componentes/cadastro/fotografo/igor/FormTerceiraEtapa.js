import React, { Component } from 'react';
// import api from '../../../../services/api'
import api from '../../../../services/api'

export class FormTerceiraEtapa extends Component {

	proxima = e => {
		e.preventDefault();
		this.props.proximaEtapa();
	};

	voltar = e => {
		e.preventDefault();
		this.props.etapaAnterior();
	};

	handleCheckbox = e => {
		e.preventDefault();
		this.props.handleCheckbox();
	}

	handleChecked = e => {
		e.preventDefault();
		this.props.handleChecked();
	}

	componentDidMountFunction = e => {
		e.preventDefault();
		this.props.componentDidMountFunction();
	}

	handleCheckbox = e => {
		e.preventDefault();
		this.props.handleCheckbox();
	}

	componentDidMount = () => {
		// e.preventDefault();
		this.props.componentDidMountFunction();
	}
	
	render() {
		
		// const { values, handleChange } = this.props;
		const isChecked = this.props.isChecked
		const especialidades = [this.props.especialidades]

		return (
			<div>
				{especialidades.map((especialidade, index) => (
					<div className="col s6" key={index}>
						<label>
							<input type="checkbox" checked={isChecked} value={especialidade.id} onChange={(e) => this.handleCheckbox(e, especialidade)} />
							<span className="color-black">{especialidade}</span>
						</label>
					</div>
				))}
				<button style={{ marginBottom: '1.9em', marginRight: '1.5em' }} onClick={this.proxima} className="waves-effect waves-light btn right">
					<i className="material-icons right">
						arrow_forward
					</i>Próxima
				</button>

				<button style={{ marginBottom: '1.9em', marginRight: '7em' }} onClick={this.voltar} className="waves-effect waves-light btn right">
					<i className="material-icons left">
						arrow_back
					</i>Anterior
				</button>
			</div>

		);
	}
}

export default FormTerceiraEtapa;