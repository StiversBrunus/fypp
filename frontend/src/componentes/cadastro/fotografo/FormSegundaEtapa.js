import React, { Component } from 'react';

export class FormSegundaEtapa extends Component {

    proximaEtapa = e => {
        e.preventDefault();
        this.props.proximaEtapa();
    };


    voltar = e => {
        e.preventDefault();
        this.props.etapaAnterior();
    };



    render() {
        const { handleChange } = this.props;
        return (
            <div id="coluna_dados_cadastro" className="col s6 center" style={{ paddingTop: '1em', backgroundColor: '#ffffff', borderTopRightRadius: '0.5em', borderBottomRightRadius: '0.5em' }}>
                <h4 style={{ marginBottom: '1em' }}>
                    WOW! Nós também amamos<br></br>
                    fotografia! Conte-nos um<br></br>
                    pouquinho sobre você.
                </h4>

                <div className="row">
                    <form className="col s12">
                        <div className="row">
                            <div className="input-field col s12">
                                <input id="cpf" type="text" onChange={handleChange('cpf')} name="cpf" type="text" className="validate"></input>
                                <label htmlFor="cpf">CPF:</label>
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field col s12">
                                <input id="email" type="text" onChange={handleChange('email')} className="validate"></input>
                                <label htmlFor="email">Email:</label>
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field col s12">
                                <input id="senha" type="password" onChange={handleChange('senha')} className="validate"></input>
                                <label htmlFor="senha">Senha:</label>
                            </div>
                        </div>

                        <button style={{ marginBottom: '1.9em', marginRight: '1.5em' }} onClick={this.proximaEtapa} className="waves-effect waves-light btn right">
                            <i className="material-icons right">
                                arrow_forward
                            </i>Próximo
                        </button>

                        <button style={{ marginBottom: '1.9em', marginRight: '7em' }} onClick={this.voltar} className="waves-effect waves-light btn right">
                            <i className="material-icons left">
                                arrow_back
                            </i>Anterior
                        </button>
                    </form>
                </div>
            </div>
        );
    }
}

export default FormSegundaEtapa;