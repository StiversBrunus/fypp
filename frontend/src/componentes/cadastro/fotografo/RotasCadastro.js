import React, { Component } from 'react';
import FormPrimeiraEtapa from './FormPrimeiraEtapa';
import FormTerceiraEtapa from './FormTerceiraEtapa';
import FormSegundaEtapa from './FormSegundaEtapa';
import FormQuartaEtapa from './FormQuartaEtapa'
import api from '../../../services/api'
import $ from 'jquery';

class RotasCadastro extends Component {

	constructor(props) {
		super(props)

		this.stateInicial = {
			etapa: 1,
			nome: '',
			cep: '',
			telefone: '',
			especialidades: [],
			selected: [],
			isChecked: false,
			cpf: '',
			email: '',
			senha: ''
			
		};

		this.state = this.stateInicial
		this.handleChecked = this.handleChecked.bind(this)
	}
		

	proximaEtapa = () => {
		const { etapa } = this.state;
		this.setState({
			etapa: etapa + 1
		});
	};

	etapaAnterior = () => {
		const { etapa } = this.state;
		this.setState({
			etapa: etapa - 1
		});
	};

	//Console
	mostrarDados = () => {
		console.log(
			'nome:', this.state.nome,
			'cep:', this.state.cep,
			'telefone:', this.state.telefone,
			'cpf:', this.state.cpf,
			'email:', this.state.email,
			'nome:', this.state.senha)
	}	

	handleImageChange =  (event) => {	
		let file = event.target.files[0];
		let reader = new FileReader();		
		reader.readAsDataURL(file);

		reader.onloadend = () => {
			console.warn("imagem data", reader.result)			
			$('#fotoDePerfil').attr('src', reader.result);
		}		
	}

	cadastrar = async (e) => {
		// e.preventDefault()
		let dados = {}
		let id = 1
		// const id = [{'id':

		// const validado = this.validacao();

		// if (validado) {

			dados = {
				nome: this.state.nome,
				cpf: this.state.cpf,
				telefone: this.state.telefone,
				cep: this.state.cep,				
				email: this.state.email,
				senha: this.state.senha,
				especialidades: this.state.especialidades
				// especialidades: [{id}]
			}

			console.log(dados)
		// }
		await api.post('/photo/fotografo', dados

		).then((res) => {
			console.log(res)
			this.setState(this.stateInicial)
		})
			.catch(erro => {
				console.log(dados)
				console.log(JSON.stringify(erro))
				console.log(erro)
			})
	}

	handleChecked() {
		this.setState({ isChecked: !this.setState.isChecked })
		console.log(this.state.isChecked)
	}

	componentDidMountFunction = () => {
		api.get('/photo/especialidades/').then(res => this.setState({ especialidades: res.data.content }))
		// console.log('entrou aq')
	}

	handleCheckbox = (e, esp) => {
		// console.log(e, esp)

		const selected = [...this.state.selected];
		console.log(selected)
		if (e.target.checked) {
			selected.push(esp)
			console.log(selected)
			
		} else {
			const index = selected.findIndex((ch) => ch.roomId === esp.roomId);
			selected.splice(index, 1);
			console.log(selected)
		}
		// this.handleChecked()
		this.setState({ selected });
	}

	// Input Handler
	handleChange = input => e => {
		this.setState({ [input]: e.target.value });
	};

	render() {
		const { etapa } = this.state;
		const { nome, cep, telefone, cpf, email, senha, especialidades } = this.state;
		const values = { nome, cep, telefone, cpf, email, senha, especialidades };

		switch (etapa) {
			case 1:
				return (
					<FormPrimeiraEtapa
						proximaEtapa={this.proximaEtapa}
						handleChange={this.handleChange}
						values={values}
					/>
				);
			case 2:
				return (
					<FormSegundaEtapa
						proximaEtapa={this.proximaEtapa}
						// mostrarDados={this.mostrarDados}
						etapaAnterior={this.etapaAnterior}
						handleChange={this.handleChange}
						values={values}
						// handleCheckbox={this.handleCheckbox}
						// cadastrar={this.cadastrar}
					/>
				);
			case 3:
				return (	
					<FormTerceiraEtapa
						proximaEtapa={this.proximaEtapa}
						etapaAnterior={this.etapaAnterior}
						handleChange={this.handleChange}
						handleChecked={this.handleChecked}						
						componentDidMountFunction={this.componentDidMountFunction}
						handleCheckbox={this.handleCheckbox}
						especialidades={this.state.especialidades}
						isChecked={this.state.isChecked}
						values={values}
					/>
					// <FormSegundaEtapa
					// 	proximaEtapa={this.mostrarDados}
					// 	etapaAnterior={this.etapaAnterior}
					// 	handleChange={this.handleChange}
					// 	values={values}
					// />
				);
			case 4:
				return(
					<FormQuartaEtapa
						cadastrar={this.cadastrar}
						etapaAnterior={this.etapaAnterior}
						handleChange={this.handleChange}
						handleImageChange={this.handleImageChange}
						// values={values}
					/>
				)

			default:
				(alert('error'))
		}
	}
}





export default RotasCadastro;
