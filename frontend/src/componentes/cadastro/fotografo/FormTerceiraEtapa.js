import React, { Component } from 'react';
// import api from '../../../../services/api'
// import api from '../../../../services/api'

export class FormTerceiraEtapa extends Component {

	proxima = e => {
		e.preventDefault();
		this.props.proximaEtapa();
	};

	voltar = e => {
		e.preventDefault();
		this.props.etapaAnterior();
	};

	handleCheckbox = (e, especialidade) => {
		e.preventDefault();
		this.props.handleCheckbox(e, especialidade);
	}

	handleChecked = e => {
		e.preventDefault();
		this.props.handleChecked();
	}

	componentDidMountFunction = e => {
		e.preventDefault();
		this.props.componentDidMountFunction();
	}

	componentDidMount() {
		// e.preventDefault();
		this.props.componentDidMountFunction();
	}

	render() {

		// const { values, handleChange } = this.props;
		const { isChecked } = this.props
		const { especialidades } = this.props

		return (
			<div id="coluna_dados_cadastro" className="col s6 center" style={{ paddingTop: '1em', backgroundColor: '#ffffff', borderTopRightRadius: '0.5em', borderBottomRightRadius: '0.5em' }}>
				<h4 style={{ marginBottom: '1em' }}>
					WOW! Nós também amamos<br></br>
                    fotografia! Conte-nos um<br></br>
                    pouquinho sobre você.
                </h4>

				<div className="row">
					<form className="col s12">
						{especialidades.map((especialidade, index) => (
							<div className="col s6" key={index}>
								<label>
									<input type="checkbox" checked={isChecked} value={especialidade.id} onChange={(e) => this.handleCheckbox(e, especialidade)} />
									<span className="color-black">{especialidade.nome}</span>
								</label>
							</div>
						))}
						<button style={{ marginBottom: '1.9em', marginRight: '1.5em' }} onClick={this.proxima} className="waves-effect waves-light btn right">
							<i className="material-icons right">
								arrow_forward
							</i>Próxima
						</button>

						<button style={{ marginBottom: '1.9em', marginRight: '7em' }} onClick={this.voltar} className="waves-effect waves-light btn right">
							<i className="material-icons left">
								arrow_back
							</i>Anterior
						</button>
					</form>
				</div>
			</div>

		);
	}
}

export default FormTerceiraEtapa;