import React, { Component } from 'react';
import InputMask from 'react-input-mask';
import api from '../../../services/api'
import './css/custom.css'



class FormCadastroFoto extends Component {

  constructor(props) {
    super(props)

    this.stateInicial = {
      nome: '',
      cpf: '',
      cep: '',
      telefone: '',
      email: '',
      senha: '',
      errosCpfCaixaVazia: "",
      errosCepCaixaVazia: "",
      erroMinimoCaracter: "",
      erroMaximoCaracter: "",
      erroNumero: "",
      erroCelular: "",
      erroEmailInvalido: '',
      erroSenha: '',
      isChecked: false,
      especialidades: [],
      selected: [],
      erroEsp: ''
    }
    this.state = this.stateInicial
    this.handleChecked = this.handleChecked.bind(this)

  }

  inputHandler = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }

  handleChecked() {
    this.setState({ isChecked: !this.setState.isChecked })
  }

  handleCheckbox = (e, esp) => {
    const selected = [...this.state.selected];
    if (e.target.checked) {
      selected.push(esp)
      console.log(selected)
    } else {
      const index = selected.findIndex((ch) => ch.roomId === esp.roomId);
      selected.splice(index, 1);
      console.log(selected)
    }
    this.setState({ selected });
  }

  componentDidMount() {
    api.get('/photo/especialidades').then(res => this.setState({ especialidades: res.data }))
  }


  validacao = () => {
    let errosCpfCaixaVazia = "";
    let errosCepCaixaVazia = "";
    let erroMinimoCaracter = "";
    let erroMaximoCaracter = "";
    let erroNumero = "";
    let erroCelular = "";
    let erroEmailInvalido = '';
    let erroSenha = '';
    let erroEsp = '';

    if (this.state.nome.length < 3 || this.state.nome.length == '') {
      erroMinimoCaracter = 'Digite seu nome, no minimo 3 caracteres!'
    }

    if (this.state.cpf.length == '') {
      errosCpfCaixaVazia = ('Digite o seu Cpf!');
    }
    if (this.state.cep == 0) {
      errosCepCaixaVazia = 'Digite o seu Cep!';
    }
    if (this.state.telefone == 0) {
      erroCelular = 'Digite o seu telefone!';
    }
    if (this.state.nome.length >= 100) {
      erroMaximoCaracter = ('Digite seu nome, no maximo 100 caracteres!"')
    }
    if (!this.state.email.includes('@') || !this.state.email.includes('.com')) {
      erroEmailInvalido = 'E-mail incorreto!'
    }


    if (this.state.senha.length > 8 || this.state.senha.length == 0) {
      erroSenha = ('Digite sua senha, no maximo 8 caracteres!"')
    }


    if (!this.state.nome.match(/^[a-zA-Z ]*$/)) {
      erroNumero = ('Digite apenas caracteres, números não permitido!"')
    }

    if (this.state.selected.length == 0) {
      erroEsp = ("Escolha a sua especialidade!")
    }

    if (erroMinimoCaracter
      || erroMaximoCaracter || erroNumero
      || errosCepCaixaVazia || erroCelular || erroEmailInvalido || erroSenha || errosCpfCaixaVazia || erroEsp
    ) {
      this.setState({
        erroMinimoCaracter, erroMaximoCaracter,
        erroNumero, errosCepCaixaVazia, erroCelular, erroEmailInvalido, erroSenha, errosCpfCaixaVazia, erroEsp
      });
      console.log(
        this.state.nome,
        this.state.cpf,
        this.state.cep,
        this.state.telefone,
        this.state.email,
        this.state.senha,
        this.state.selected.length
      )
      return false
    }


    return true


  }

  cadastrar = async (e) => {
    e.preventDefault()
    let dados = {}

    const validado = this.validacao();

    if (validado) {

      dados = {
        nome: this.state.nome,
        cpf: this.state.cpf,
        cep: this.state.cep,
        telefone: this.state.telefone,
        email: this.state.email,
        senha: this.state.senha,
        especialidades: this.state.selected
      }

      console.log(dados)
    }
    await api.post('/photo/fotografo', dados

    ).then((res) => {
      console.log(res)
      this.setState(this.stateInicial)
    })
      .catch(erro => {
        console.log(JSON.stringify(erro))
        console.log(erro)
      })

  }


  render() {
    const { nome, cpf, cep, telefone, email, senha, especialidades } = this.state;

    return (
      <body>
        <div className="container center ">
          <div className="row ">
            <div className="col s11 offset-s1">
              <div className="card z-depth-3">
                <h3 className="titulo">Cadastro</h3>
                <div className="row">
                  <div className="col s10 offset-s1">
                    <div className=" card-content white">
                      <form className="left-align">
                        <div className="  errosFormNome" style={{ color: 'red' }} >{this.state.errosCaixaVazia}</div>
                        <div className=" errosFormNome" style={{ color: 'red' }}>{this.state.erroMinimoCaracter}</div>
                        <div className=" errosFormNome" style={{ color: 'red' }}>{this.state.erroMaximoCaracter}</div>
                        <div className="errosFormNome" style={{ color: 'red' }}>{this.state.erroNumero}</div>
                        <div class="row" >
                          <p>
                            <label for="first_name" className="font-13 color-black">Nome:</label>
                          </p>
                          <div className="input-field col s12 border-purple">
                            <input id="nome" type="text" value={nome} name="nome" onChange={this.inputHandler} className="validate " />
                          </div>
                        </div>
                      </form>
                      <div class="row margin-top" >
                        <div class="col s6">
                          <form className="left-align margin-right">
                            <div class="row " >
                              <p>
                                <div className="col s10 offset-s1 errosForm" style={{ color: 'red' }}>{this.state.errosCpfCaixaVazia}</div>
                                <label for="first_name" className="font-13 color-black">CPF:  </label>
                              </p>
                              <div className="input-field col s12 border-purple ">
                                <InputMask type="text" name="cpf" id="cpf" name="cpf" value={cpf} onChange={this.inputHandler} className="validate " mask="999.999.999-99" />
                              </div>
                            </div>
                          </form>
                        </div>
                        <div class="col s6">
                          <form className="left-align">
                            <div class="row " >
                              <p>
                                <div className="col s10 offset-s1 errosForm" style={{ color: 'red' }}>{this.state.errosCepCaixaVazia}</div>
                                <label for="first_name" className="font-13 color-black">CEP:</label>
                              </p>
                              <div className="input-field col s12 border-purple">
                                <InputMask type="text" name="cep" id="cep" name="cep" onChange={this.inputHandler} value={cep} className="validate " mask="99999-999" />
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                      <form className="left-align margin-right">
                        <div class="row margin-top" >
                          <p>
                            <div className="col s10 offset-s2 errosForm" style={{ color: 'red' }}>{this.state.erroCelular}</div>
                            <label for="first_name" className="font-13 color-black">Telefone:</label>
                          </p>
                          <div className="input-field col s12 border-purple">
                            <InputMask type="text" name="telefone" id="telefone" name="telefone" value={telefone} onChange={this.inputHandler} className="validate " mask="(99) 99999-9999" />
                          </div>
                        </div>
                      </form>
                      <form className="left-align">

                        <div class="row margin-top" >
                          <p>
                          <div className="col s10 offset-s3 errosForm" style={{ color: 'red' }}>{this.state.erroEsp}</div>
                            <label for="first_name" className="font-13 color-black">Especialidades:</label>
                          </p>
                          {especialidades.map((especialidade, index) => (
                            <div class="col s6" key={index}>
                              <label >
                                <input type="checkbox" checked={this.isChecked} value={especialidade.id} onChange={(e) => this.handleCheckbox(e, especialidade)} />
                                <span className="color-black">{especialidade.nome}</span>

                              </label>
                            </div>
                          ))}
                        </div>
                      </form>
                      <form className="left-align">
                        <div class="row margin-top" >
                          <p>
                            <div className="col s10 offset-s1 errosForm" style={{ color: 'red' }}>{this.state.erroEmailInvalido}</div>
                            <label for="first_name" className="font-13 color-black" >E-mail:</label>
                          </p>
                          <div className="input-field col s12 border-purple">
                            <input id="email" type="email" className="validate " name="email" value={email} onChange={this.inputHandler} />
                          </div>
                        </div>
                      </form>
                      <form className="left-align margin-right">
                        <div class="row margin-top" >
                          <p>
                            <div className="col s4 offset-s1 errosForm" style={{ color: 'red' }}>{this.state.erroSenha}</div>
                            <label for="first_name" className="font-13 color-black" >Senha:</label>
                          </p>
                          <div className="input-field col s12 border-purple">
                            <input id="password" type="password" class="validate" name="senha" value={senha} onChange={this.inputHandler} />
                          </div>
                        </div>
                      </form>

                      
                      <div className="row">
                        <div className="col s12 center-align">
                          <button class="btn-large" type="button" onClick={this.cadastrar}>
                            Cadastrar
                        </button>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </body>

    );
  }

}


export default FormCadastroFoto;