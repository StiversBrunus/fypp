import React, { Component } from 'react';
import FormPrimeiraEtapa from './FormPrimeiraEtapa';
import FormSegundaEtapa from './FormSegundaEtapa';
import FormTerceiraEtapa from './FormTerceiraEtapa';
import api from '../../../services/api'
import Swal from 'sweetalert2'
import $ from 'jquery';
import M from 'materialize-css'

class RotasCadastro extends Component {
	constructor(props) {
		super(props)

		this.stateInicial = {
			etapa: 1,
			nome: '',
			cep: '',
			telefone: '',
			email: '',
			senha: '',
			cpf: '',
			base64: '',
			errosCepCaixaVazia: '',
			erroMinimoCaracter: '',
			erroMaximoCaracter: '',
			erroNumero: '',
			erroCelular: '',
			erroEmailInvalido: '',
			erroSenha: '',
			erroCpf: '',
			erroCpfIncompleto: ''
		}
		this.state = this.stateInicial
	}

	//Função que leva para a proxima etapa do formulario
	proximaEtapa = () => {
		const { etapa } = this.state;
		this.setState({
			etapa: etapa + 1
		});
	};

	//Função que leva para a etapa anterior do formulario
	etapaAnterior = () => {
		const { etapa } = this.state;
		this.setState({
			etapa: etapa - 1
		});
	};

	//validação da primeira etapa do formulario
	validacaoPrimeiroForm = () => {
		let erroMinimoCaracter = "";
		let errosCepCaixaVazia = "";
		let erroMaximoCaracter = "";
		let erroNumero = "";
		let erroCelular = "";

		if (this.state.nome.length < 3 || this.state.nome.length == '') {
			erroMinimoCaracter = 'Dígite seu nome, no mínimo 3 caracteres!'
		}
		if (this.state.cep == 0) {
			errosCepCaixaVazia = 'Dígite o seu Cep!';
		}
		if (this.state.cep == 0 || this.state.cep.length <= 8) {
			errosCepCaixaVazia = 'Dígite o seu Cep!';
		}
		if (this.state.telefone == 0 || this.state.telefone.length == 1) {
			erroCelular = 'Dígite o seu telefone!';
		}
		if (!this.state.nome.match(/^[a-zA-Z ]*$/)) {
			erroNumero = 'Dígite apenas letras, números não são permitido!'
		}

		if (erroMinimoCaracter || erroMaximoCaracter || erroNumero || errosCepCaixaVazia || erroCelular) {
			if (erroMinimoCaracter) {
				this.setState({ erroMinimoCaracter });
				M.toast({ html: this.state.erroMinimoCaracter })
			}

			if (erroMaximoCaracter) {
				this.setState({ erroMaximoCaracter });
				M.toast({ html: this.state.erroMaximoCaracter })
			}

			if (erroNumero) {
				this.setState({ erroNumero });
				M.toast({ html: this.state.erroNumero })
			}

			if (errosCepCaixaVazia) {
				this.setState({ errosCepCaixaVazia });
				M.toast({ html: this.state.errosCepCaixaVazia })
			}

			if (erroCelular) {
				this.setState({ erroCelular });
				M.toast({ html: this.state.erroCelular })
			}

			return false
		}

		return true
	}

	//validação da segunda etapa do formulario
	validacaoSegundoForm = () => {
		let erroEmailInvalido = '';
		let erroSenha = '';
		let erroCpf = '';
		let erroCpfIncompleto = ''
		const M = window.M;

		if (!this.state.email.includes('@') || !this.state.email.includes('.com')) {
			erroEmailInvalido = 'E-mail incorreto!'
		}

		if (this.state.senha.length < 8 || this.state.senha.length == 0) {
			erroSenha = ('Dígite sua senha, no mínimo 8 caracteres!')
		}

		if (this.state.cpf == 0) {
			erroCpf = 'Por Favor, dígite o seu cpf!';
		}

		if (this.state.cpf.length != 14) {
			erroCpfIncompleto = 'Por Favor, dígite um cpf válido!';
		}

		if (erroEmailInvalido || erroSenha || erroCpf || erroCpfIncompleto) {
			if (erroEmailInvalido) {
				this.setState({ erroEmailInvalido });
				M.toast({ html: this.state.erroEmailInvalido })
			}

			if (erroSenha) {
				this.setState({ erroSenha });
				M.toast({ html: this.state.erroSenha })
			}

			if (erroCpfIncompleto) {
				this.setState({ erroCpfIncompleto });
				M.toast({ html: this.state.erroCpfIncompleto })
			}

			if (erroCpf) {
				this.setState({ erroCpf });
				M.toast({ html: this.state.erroCpf })
			}

			return false
		}

		return true
	}

	//função que guarda a imagem no banco 
	handleImageChange = (event) => {

		let file = event.target.files[0];

		let reader = new FileReader();
		reader.readAsDataURL(file);

		//colocando a imagem como preview na div
		reader.onloadend = () => {
			// console.warn("imagem data", reader.result);
			$('#fotoDePerfil').attr('src', reader.result);
		}
	}

	cadastrar = async () => {

		let dados = {}

		dados = {
			nome: this.state.nome,
			cep: this.state.cep,
			telefone: this.state.telefone,
			email: this.state.email,
			senha: this.state.senha,
			fotoPerfil: this.state.base64
		}
		console.log(dados)

		await api.post('/photo/cliente', dados).then((res) => {
			console.log(res)
			this.setState(this.stateInicial)
			this.alertSucess()
		}).catch(erro => {
			console.log(JSON.stringify(erro))
			console.log(dados)
			this.alertError()
			console.log(erro)
		})
	}

	//alertas
	alertSucess = () => {
		Swal.fire(
			'Obrigado!',
			'Sua conta foi criada com sucesso',
			'success'
		)
	}

	alertError = () => {
		Swal.fire({
			icon: 'error',
			title: 'Erro ao cadastrar',
			text: 'Tivemos algum problema com o seu cadastro, por favor tente novamente'		
		})
	}

	//inputHandler
	handleChange = input => e => {
		this.setState({ [input]: e.target.value });
	};

	render() {
		const { etapa } = this.state;
		const { nome, cep, telefone, email, senha, cpf } = this.state;
		const values = { nome, cep, telefone, email, senha, cpf };

		//switch que controla as etapas do processo de cadastro
		switch (etapa) {
			case 1:
				return (
					//Renderizando o objeto e passando propriedades para ele
					<FormPrimeiraEtapa
						proximaEtapa={this.proximaEtapa}
						handleChange={this.handleChange}
						validacao={this.validacaoPrimeiroForm}
						values={values}
					/>
				);
			case 2:
				return (
					<FormSegundaEtapa
						etapaAnterior={this.etapaAnterior}
						proximaEtapa={this.proximaEtapa}
						handleChange={this.handleChange}
						validacao={this.validacaoSegundoForm}
						values={values}
					/>					
				);
			case 3:
				return (
					<FormTerceiraEtapa
						cadastrar={this.cadastrar}
						etapaAnterior={this.etapaAnterior}
						handleChange={this.handleChange}
						handleImageChange={this.handleImageChange}
					/>
				);
			default:
				(alert('error'))
		}
	}
}

export default RotasCadastro;
