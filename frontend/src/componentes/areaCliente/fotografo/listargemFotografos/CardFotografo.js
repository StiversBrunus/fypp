import React, { Component } from 'react'
import api from '../../../../services/api'
import { Link } from 'react-router-dom'
import axios from 'axios'
import Harry from '../../categoria/imagens/harry2.jpg'

class CardFotografo extends Component {

    alertError = e => {
        // e.preventDefault()
        this.props.alertError();
    }

    state = {
        fotografos: [],
        // loading: this.props.loading
        // localidade: [],
        // uf: []
    }
    
    componentDidMount = async () => {
        let token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbkBhZG1pbi5jb20iLCJyb2xlcyI6WyJBRE1JTiJdLCJpYXQiOjE1OTEyNzA5NDEsImV4cCI6MTU5MTM1NzM0MX0.du69f5OTlkCmc6J6okqGdLqLy_xKq4XxESUwuW_mkFpNNa8w-8Mt7-f3pFt_zCJ6TDBY9KYqc-URMvr14BPSog"
        let config = {
            headers: {
                'authorization': token,
                'Access-Control-Allow-Origin': '*'
            }
        }
    
        await api.get('/photo/fotografos'
        // , config
        )
            .then((res) => {
                console.log(res.data.content)
                this.setState({
                    fotografos: res.data.content
                })

            }).catch(erro => {
                console.log(erro)
                this.alertError();
            })    
    }
    
    //FUNCTION QUE CONSOME A API DO VIA CEP PARA DESCOBRIR A CIDADE E ESTADO DO FOTOGRAFO
    buscarCidade = (cep) => {     
        let localidade = ''
        // let uf   
        let ceps = cep;
        console.log('ceps:', cep)
        let resultado = ceps.replace('-', '')
        console.log(resultado)
    
        axios.get('https://viacep.com.br/ws/'+ resultado +'/json/')
            .then((res) => {
                console.log(res.data.localidade)
                console.log(res.data.uf)
    
                // this.setState({
                    localidade = res.data.localidade
                //     uf: res.data.uf
                // })
            }).catch(erro =>{
                console.log(erro)
            })
    
          return localidade
        
    }

    render() {
        const fotografos = this.state.fotografos;
        let localidade = ''

        //CARD COM AS INFORMAÇÕES BASICAS DOS FOTOGRAFOS
        return (
            <div>
                {fotografos.map((fotografo, index) => (
                    <div className="col s12 m6 l6" key={index}>
                        <div className="card">
                            <div className="card-content white-text " style={{cursor:"default"}}>
                                <div className="row valign-wrapper">
                                    <div className="col s4 center">
                                        <img className="circle responsive-img" src={Harry}/>
                                    </div>
                                    <div className="col s8 center">
                                        <p style={{ color: '#641682', fontFamily: 'Montserrat-medium', textTransform:'capitalize'}}>{fotografo.nome}, 18</p>
                                        { localidade = this.buscarCidade(fotografo.cep) }
                                        {/* {console.log('essa', localidade)} */}
                                        <p style={{ color: '#000000', fontFamily: 'Montserrat-medium', textTransform:'capitalize'}}>{fotografo.experiencia}</p>
                                        <p style={{ color: 'orange', fontSize: '15px' }}> ★ ★ ★ ★ ★</p>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="card-action right">
                                    <Link to={"/cliente/fotografos/" + fotografo.id} style={{ color: '#641682' }}>detalhes</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        )
    }
}

export default CardFotografo 
