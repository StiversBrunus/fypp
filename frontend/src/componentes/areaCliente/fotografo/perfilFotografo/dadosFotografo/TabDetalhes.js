import React, { Component } from 'react'
import M from 'materialize-css'

class TabDetalhes extends Component {

    componentDidMount(){
        let el = document.querySelector('.tabs')
        let intance = M.Tabs.init(el, {
            swipeable: 'true'
        })
    }

    render() {
        return (
            <div>
                <div className="col s8">
                    <div class="row">
                        <div class="col s12">
                            <ul class="tabs">
                                <li class="tab col s3"><a class="active" href="#orcamentosFotografo">Planos</a></li>
                                <li class="tab col s3"><a href="#test2">Port. 01</a></li>
                                <li class="tab col s3"><a href="#test3">Port. 02</a></li>
                                <li class="tab col s3"><a href="#test4">Port. 03</a></li>
                            </ul>
                        </div>
                        <div id="orcamentosFotografo" class="col s12" style={{backgroundColor:'red'}}>Planos</div>
                        <div id="test2" class="col s12" style={{backgroundColor:'blue'}}>01</div>
                        <div id="test3" class="col s12" style={{backgroundColor:'green'}}>02</div>
                        <div id="test4" class="col s12" style={{backgroundColor:'orange'}}>03</div>
                    </div>
                </div>
            </div>
        )
    }
}

export default TabDetalhes
