import React, { Component } from 'react'
import Harry from '../../../categoria/imagens/harry2.jpg'
import api from '../../../../../services/api'
import axios from 'axios'
import '../style.css'
import TabDetalhes from './TabDetalhes'


export default class DadosPerfilFotografo extends Component {

    state = {
        fotografo: [],
        nome: '',
        idade: '',
        cidade: '',
        experiencia: '',
        avaliacao: '',
        nota: '',
        localidade: '',
        uf: ''
    }

    componentDidMount = async () => {
        

        const { idFotografo } = this.props

        let token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbkBhZG1pbi5jb20iLCJyb2xlcyI6WyJBRE1JTiJdLCJpYXQiOjE1OTEyNzA5NDEsImV4cCI6MTU5MTM1NzM0MX0.du69f5OTlkCmc6J6okqGdLqLy_xKq4XxESUwuW_mkFpNNa8w-8Mt7-f3pFt_zCJ6TDBY9KYqc-URMvr14BPSog"
        let config = {
            headers: {
                'authorization': token,
                'Access-Control-Allow-Origin': '*'
            }
        }

        await api.get('/photo/fotografos/' + idFotografo)
            .then((res) => {
                this.buscarCidade(res.data.cep)
                console.log(res.data)
                this.setState({
                    nome: res.data.nome,
                    experiencia: res.data.experiencia
                })
                
            }).catch(erro => {
                console.log(erro)
            })
    }

    buscarCidade = (cep) => {
        let ceps = cep;
        let resultado = ceps.replace('-', '')

        axios.get('https://viacep.com.br/ws/' + resultado + '/json/')
            .then((res) => {
                this.setState({
                    localidade: res.data.localidade,
                    uf: res.data.uf
                })
            }).catch(erro => {
                console.log(erro)
            })
    }


    render() {
        // const { idFotografo } = this.props
        // const fotografoDados = this.state.fotografo

        return (
            <div className="row" style={{ marginBottom: '0', paddingBottom: '0' }}>
                <div className="container" style={{ marginBottom: '0' }}>
                    <div className="col s4" style={{ backgroundColor: "#641682", height: '71vh', paddingTop: '1em' }}>
                        <img class="circle" style={{ position: 'absolute', marginTop: '-8.5em', marginLeft: '2em' }} width="200" src={Harry} />
                        <div style={{ paddingTop: '5em' }}>
                            <div className="row center" style={{ marginBottom: '1em' }}>
                                <p style={{ color: '#ffffff', fontSize: '18px', fontFamily: 'Montserrat-extrabold', letterSpacing: '1px', textTransform: 'capitalize', marginBottom: '0' }}>{this.state.nome}, 18</p>
                                <p style={{ color: '#ffffff', fontSize: '15px', fontFamily: 'Montserrat-medium', textTransform: 'capitalize', marginTop: '0' }}>{this.state.localidade}, {this.state.uf}</p>
                            </div>

                            <div className="row">
                                <div className="col s12">
                                    <p style={{ color: '#270833', marginLeft: '1.2em', fontSize: '15px', fontFamily: 'Montserrat-medium', fontWeight: 'bold', textTransform: 'capitalize', marginBottom: '0' }}>categoria</p>
                                    <p style={{ color: '#ffffff', marginLeft: '1em', fontSize: '17px', fontFamily: 'Montserrat-medium', textTransform: 'capitalize', marginTop: '0' }}>{this.state.experiencia}</p>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col s12">
                                    <p style={{ color: '#270833', marginLeft: '1.2em', fontSize: '15px', fontFamily: 'Montserrat-medium', fontWeight: 'bold', textTransform: 'capitalize', marginBottom: '0' }}>Experiência</p>
                                    <p style={{ color: '#ffffff', marginLeft: '1em', fontSize: '17px', fontFamily: 'Montserrat-medium', textTransform: 'capitalize', marginTop: '0' }}>{this.state.experiencia}</p>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col s12">
                                    <p style={{ color: '#270833', marginLeft: '1.2em', fontSize: '15px', fontFamily: 'Montserrat-medium', fontWeight: 'bold', textTransform: 'capitalize', marginBottom: '0' }}>Nota</p>
                                    <p style={{ color: '#ffffff', marginLeft: '1em', fontSize: '17px', fontFamily: 'Montserrat-medium', textTransform: 'capitalize', marginTop: '0' }}>★ ★ ★ ★ ★</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <TabDetalhes/>
                </div>
            </div>
        )
    }
}
