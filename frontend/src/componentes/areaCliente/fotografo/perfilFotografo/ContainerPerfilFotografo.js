import React, { Component } from 'react'
import { Navbar, BannerBotao } from './Navbar'
import DadosPerfilFotografo from './dadosFotografo/DadosPerfilFotografo'

class ContainerPerfilFotografo extends Component {
    constructor(props){
        super(props);
    }

    state = {
        idFotografo: ''
    }

    componentDidMount () {
        const id = this.props.match.params.idFotografo

        this.setState({
            idFotografo: id
        })
    }    
    
    render() {
        return (
            <div className="row" style={{marginBottom:'0'}}>
                <Navbar/>
                <BannerBotao idFotografo={this.props.match.params.idFotografo}/>
                <DadosPerfilFotografo idFotografo={this.props.match.params.idFotografo} />
            </div>
        )
    }
}

export default ContainerPerfilFotografo;
