import React, { Component } from 'react'
import api from '../../../services/api'
import { Link } from 'react-router-dom'

class CardCategoria extends Component {

    alertError = () => {
        // e.preventDefault()
        this.props.alertError();
    }

    state = {
        categorias: []
    }

    componentDidMount = async () => {
        let token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbkBhZG1pbi5jb20iLCJyb2xlcyI6WyJBRE1JTiJdLCJpYXQiOjE1OTEyNzA5NDEsImV4cCI6MTU5MTM1NzM0MX0.du69f5OTlkCmc6J6okqGdLqLy_xKq4XxESUwuW_mkFpNNa8w-8Mt7-f3pFt_zCJ6TDBY9KYqc-URMvr14BPSog"
        let config = {
            headers: {
                'authorization': token,
                'Access-Control-Allow-Origin': '*'
            }
        }

        await api.get('/photo/especialidades'
            // , config
        )
            .then((res) => {
                // console.log(res.data)
                this.setState({
                    categorias: res.data.content
                })
            }).catch(erro => {
                console.log(erro)
                this.alertError();
            })

    }

    render() {
        const categorias = this.state.categorias;

        return (
            <div>
                {categorias.map((categoria, index) => (
                    <div class="col s12 m8 l3" key={index} >
                        <div class="card-panel waves-effect waves-block waves-light">
                            <div class="row valign-wrapper">
                                <div class="col l12" style={{ marginLeft: 'auto', marginRight: 'auto' }}>
                                    <img src={categoria.imagem} style={{ height: '110px' }} alt="" class="circle responsive-img" />
                                </div>
                            </div>
                            <div class="row valign-wrapper">
                                <div class="col l12 center">
                                    <Link to={"/cliente/categorias/" + categoria.id + "/data"} style={{ fontFamily: 'Montserrat-regular' }}>{categoria.nome}</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        )
    }
}

export default CardCategoria
