package br.senai.sp.backend.resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.senai.sp.backend.dto.FotoPortfolioToken;
import br.senai.sp.backend.dto.PortfolioToken;
import br.senai.sp.backend.model.Cliente;
import br.senai.sp.backend.model.Portfolio;
import br.senai.sp.backend.repository.PortfolioRepository;
import br.senai.sp.backend.security.JwtAuthService;

@RestController
@RequestMapping("/photo")

@CrossOrigin
public class PortfolioResource {

	@Autowired
	private PortfolioRepository portfolioRepository;
	
	@Autowired
	private JwtAuthService jwtAuthService;
	
	//listar todos os portifolios
	@GetMapping("/portfolios")
	public Page<Portfolio> getPortfolios (Pageable paginacao){
	return portfolioRepository.findAll(paginacao);
	}
	
	//listar todos os portfolios de um fotografo
	@GetMapping("/portfolio/{id}")
	public ResponseEntity<?> getportfolioFotografo(@PathVariable Long id_fotografo) {
		Optional fotografo_id = portfolioRepository.findById(id_fotografo);
		return fotografo_id.isPresent() ? ResponseEntity.ok(fotografo_id) : ResponseEntity.notFound().build();
	}
	
	//criar portfolio
	
	@PostMapping("/portfolio")
	@ResponseStatus(HttpStatus.CREATED)
	public PortfolioToken gravar(@Valid @RequestBody Portfolio portfolio) {
		
		Portfolio novoPortfolio = new Portfolio();
		novoPortfolio= portfolioRepository.save(portfolio);
		
		PortfolioToken portfolioToken = new PortfolioToken();
		List<String> roles = new ArrayList<>();
		//String token =  jwtAuthService.createToken(portfolio.getEmail(), roles) ;
		//portfolioToken.setToken(token);
		portfolioToken.setId_fotografo(portfolio.getId_fotografo());
		portfolioToken.setId(portfolio.getId());
		portfolioToken.setNome(portfolio.getNome());
		//portfolioToken.setRole(portfolio.getRole());
		
		return portfolioToken;
}
}
