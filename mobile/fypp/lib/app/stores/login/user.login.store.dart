import 'dart:convert';

import 'package:fypp/app/utils/api.dart';
import 'package:fypp/app/utils/shared.prefs.dart';
import 'package:mobx/mobx.dart';
import 'package:http/http.dart';
import 'package:validators/validators.dart';
part 'user.login.store.g.dart';

class UserLoginStore = _UserLoginStoreBase with _$UserLoginStore;

abstract class _UserLoginStoreBase with Store {

  SharedPrefs prefs = SharedPrefs();
  
  @observable
  Map<String, dynamic> loggedUser = {};

  @observable
  String email = '';

  @observable
  String password = '';

  @observable
  bool visibility = false; 

  @observable
  bool isLoading = false;

  @observable
  bool isLoggedIn = false;

  @action
  void setEmail(String value) => email = value;

  @action
  void setPassword(String value) => password = value;

  @action
  void togglePasswordVisibility() => visibility = !visibility;

  @computed
  bool get isEmailValid => isEmail(email);

  @computed
  bool get isPasswordValid => password.length > 5;

  @computed
  Function get loginPressed => 
    (isEmailValid && isPasswordValid && !isLoading) ? login : null;  

  @action
  Future<void> login() async {
    isLoading = true;
    Response response = await post(
      Api.BASE_URL + 'auth/cliente/login',
      headers: Api.BASE_HEADER,
      body: jsonEncode({
        'email': email,
        'senha': password
      }),
    );

    if (response.statusCode == 200) {
      var body = jsonDecode(response.body);
      loggedUser = {
        'email': body['email'],
        'token': body['token']
      };
      prefs.getAndSaveToken(loggedUser['token']);
      isLoading = false;
      isLoggedIn = true;
      return loggedUser;
    }
  }

}