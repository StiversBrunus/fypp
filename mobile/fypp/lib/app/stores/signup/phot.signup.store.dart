import 'dart:convert';

import 'package:fypp/app/model/photographer.dart';
import 'package:fypp/app/utils/api.dart';
import 'package:intl/intl.dart';
import 'package:mobx/mobx.dart';
import 'package:validators/validators.dart';
import 'package:http/http.dart';
import 'package:cep_future/cep_future.dart';
part 'phot.signup.store.g.dart';

class PhotSignUpStore = _PhotSignUpStoreBase with _$PhotSignUpStore;

abstract class _PhotSignUpStoreBase with Store {
  
  @observable
  Photographer loggedPhot = Photographer();

  DateFormat dateFormat = DateFormat('dd/MM/yyyy');

  @observable
  String name = '';

  @observable
  String cpf = '';

  @observable
  DateTime birthDate;

  @observable
  String cep = '';

  @observable
  String city = '';

  @observable
  String state = '';  

  @observable
  String experience = '';

  @observable
  String phone = '';

  @observable
  String email = '';

  @observable
  String password = '';

  @observable
  bool isLoading = false;

  @observable
  bool isLoggedIn = false;

  @action
  void setName(String value) => name = value;

  @action
  void setCpf(String value) => cpf = value;

  @action
  void setBirthDate(DateTime value) => birthDate = value;

  @action
  Future<void> setCep(String value) async {
    cep = value;
    if(cep.length > 8){
      Map<String, dynamic> cepDetails = await searchCep(cep);
      city = cepDetails['city'];
      setCity(city);
      print(city);
    }
  }

  @action
  void setCity(String value) => city = value;

  @action
  void setExperience(String value) => experience = value;

  @action
  void setPhone(String value) => phone = value;

  @action
  void setEmail(String value) => email = value;

  @action
  void setPassword(String value) => password = value;

  @computed
  bool get isNameValid => !isNumeric(name);

  @computed
  bool get isCpfValid => cpf.length > 13;

  @computed
  bool get isBirthDateValid => birthDate != null;

  @computed
  bool get isCepValid => cep.length > 8;

  @computed
  bool get isExperienceValid => !isNumeric(experience);

  @computed
  bool get isPhoneValid => phone.length > 9;

  @computed
  bool get isEmailValid => isEmail(email);

  @computed
  bool get isPasswordValid => password.length > 5;

  @computed
  Function get firstFormPressed => (isNameValid &&
          isCpfValid &&
          isBirthDateValid &&
          isCepValid &&
          isExperienceValid &&
          isPhoneValid &&
          isEmailValid &&
          isPasswordValid &&
          !isLoading)
      ? signUp
      : null;

  @action
  Future<Map<String, dynamic>> searchCep(String cep) async {
    final result = await cepFuture(cep);
    Map<String, dynamic> cepDetails = {
      'city': result.city,
      'state': result.state
    };
    return cepDetails;
  } 

  @action
  Future<Photographer> signUp() async {
    isLoading = true;
    Photographer phot = Photographer(
      id: null,
      name: name,
      cpf: cpf,
      birthDate: dateFormat.format(birthDate).toString(),
      cep: cep,
      experience: experience,
      phone: phone,
      email: email,
      role: 'ADMIN',
      password: password,
    );
    Response response = await post(
      Api.BASE_URL + 'fotografo',
      headers: Api.BASE_HEADER,
      body: jsonEncode(
        phot.toJson(),
      ),
    );

    if (response.statusCode == 201) {
      await Future.delayed(Duration(seconds: 2));
      isLoading = false;
      isLoggedIn = true;
      loggedPhot = Photographer.fromJson(jsonDecode(response.body));
      return loggedPhot;
    }

    return null;
  }

}