// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.signup.store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$UserSignUpStore on _UserSignUpStoreBase, Store {
  Computed<bool> _$isNameValidComputed;

  @override
  bool get isNameValid =>
      (_$isNameValidComputed ??= Computed<bool>(() => super.isNameValid,
              name: '_UserSignUpStoreBase.isNameValid'))
          .value;
  Computed<bool> _$isPhoneValidComputed;

  @override
  bool get isPhoneValid =>
      (_$isPhoneValidComputed ??= Computed<bool>(() => super.isPhoneValid,
              name: '_UserSignUpStoreBase.isPhoneValid'))
          .value;
  Computed<bool> _$isCepValidComputed;

  @override
  bool get isCepValid =>
      (_$isCepValidComputed ??= Computed<bool>(() => super.isCepValid,
              name: '_UserSignUpStoreBase.isCepValid'))
          .value;
  Computed<bool> _$isEmailValidComputed;

  @override
  bool get isEmailValid =>
      (_$isEmailValidComputed ??= Computed<bool>(() => super.isEmailValid,
              name: '_UserSignUpStoreBase.isEmailValid'))
          .value;
  Computed<bool> _$isPasswordValidComputed;

  @override
  bool get isPasswordValid =>
      (_$isPasswordValidComputed ??= Computed<bool>(() => super.isPasswordValid,
              name: '_UserSignUpStoreBase.isPasswordValid'))
          .value;
  Computed<Function> _$firstFormPressedComputed;

  @override
  Function get firstFormPressed => (_$firstFormPressedComputed ??=
          Computed<Function>(() => super.firstFormPressed,
              name: '_UserSignUpStoreBase.firstFormPressed'))
      .value;

  final _$loggedUserAtom = Atom(name: '_UserSignUpStoreBase.loggedUser');

  @override
  User get loggedUser {
    _$loggedUserAtom.reportRead();
    return super.loggedUser;
  }

  @override
  set loggedUser(User value) {
    _$loggedUserAtom.reportWrite(value, super.loggedUser, () {
      super.loggedUser = value;
    });
  }

  final _$nameAtom = Atom(name: '_UserSignUpStoreBase.name');

  @override
  String get name {
    _$nameAtom.reportRead();
    return super.name;
  }

  @override
  set name(String value) {
    _$nameAtom.reportWrite(value, super.name, () {
      super.name = value;
    });
  }

  final _$phoneAtom = Atom(name: '_UserSignUpStoreBase.phone');

  @override
  String get phone {
    _$phoneAtom.reportRead();
    return super.phone;
  }

  @override
  set phone(String value) {
    _$phoneAtom.reportWrite(value, super.phone, () {
      super.phone = value;
    });
  }

  final _$cepAtom = Atom(name: '_UserSignUpStoreBase.cep');

  @override
  String get cep {
    _$cepAtom.reportRead();
    return super.cep;
  }

  @override
  set cep(String value) {
    _$cepAtom.reportWrite(value, super.cep, () {
      super.cep = value;
    });
  }

  final _$emailAtom = Atom(name: '_UserSignUpStoreBase.email');

  @override
  String get email {
    _$emailAtom.reportRead();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.reportWrite(value, super.email, () {
      super.email = value;
    });
  }

  final _$passwordAtom = Atom(name: '_UserSignUpStoreBase.password');

  @override
  String get password {
    _$passwordAtom.reportRead();
    return super.password;
  }

  @override
  set password(String value) {
    _$passwordAtom.reportWrite(value, super.password, () {
      super.password = value;
    });
  }

  final _$isLoadingAtom = Atom(name: '_UserSignUpStoreBase.isLoading');

  @override
  bool get isLoading {
    _$isLoadingAtom.reportRead();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.reportWrite(value, super.isLoading, () {
      super.isLoading = value;
    });
  }

  final _$isLoggedInAtom = Atom(name: '_UserSignUpStoreBase.isLoggedIn');

  @override
  bool get isLoggedIn {
    _$isLoggedInAtom.reportRead();
    return super.isLoggedIn;
  }

  @override
  set isLoggedIn(bool value) {
    _$isLoggedInAtom.reportWrite(value, super.isLoggedIn, () {
      super.isLoggedIn = value;
    });
  }

  final _$signUpAsyncAction = AsyncAction('_UserSignUpStoreBase.signUp');

  @override
  Future<User> signUp() {
    return _$signUpAsyncAction.run(() => super.signUp());
  }

  final _$_UserSignUpStoreBaseActionController =
      ActionController(name: '_UserSignUpStoreBase');

  @override
  void setName(String value) {
    final _$actionInfo = _$_UserSignUpStoreBaseActionController.startAction(
        name: '_UserSignUpStoreBase.setName');
    try {
      return super.setName(value);
    } finally {
      _$_UserSignUpStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setPhone(String value) {
    final _$actionInfo = _$_UserSignUpStoreBaseActionController.startAction(
        name: '_UserSignUpStoreBase.setPhone');
    try {
      return super.setPhone(value);
    } finally {
      _$_UserSignUpStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setCep(String value) {
    final _$actionInfo = _$_UserSignUpStoreBaseActionController.startAction(
        name: '_UserSignUpStoreBase.setCep');
    try {
      return super.setCep(value);
    } finally {
      _$_UserSignUpStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setEmail(String value) {
    final _$actionInfo = _$_UserSignUpStoreBaseActionController.startAction(
        name: '_UserSignUpStoreBase.setEmail');
    try {
      return super.setEmail(value);
    } finally {
      _$_UserSignUpStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setPassword(String value) {
    final _$actionInfo = _$_UserSignUpStoreBaseActionController.startAction(
        name: '_UserSignUpStoreBase.setPassword');
    try {
      return super.setPassword(value);
    } finally {
      _$_UserSignUpStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
loggedUser: ${loggedUser},
name: ${name},
phone: ${phone},
cep: ${cep},
email: ${email},
password: ${password},
isLoading: ${isLoading},
isLoggedIn: ${isLoggedIn},
isNameValid: ${isNameValid},
isPhoneValid: ${isPhoneValid},
isCepValid: ${isCepValid},
isEmailValid: ${isEmailValid},
isPasswordValid: ${isPasswordValid},
firstFormPressed: ${firstFormPressed}
    ''';
  }
}
