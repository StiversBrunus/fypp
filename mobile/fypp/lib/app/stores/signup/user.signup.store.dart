import 'dart:convert';

import 'package:fypp/app/model/user.dart';
import 'package:fypp/app/utils/api.dart';
import 'package:mobx/mobx.dart';
import 'package:http/http.dart';
import 'package:validators/validators.dart';
part 'user.signup.store.g.dart';

class UserSignUpStore = _UserSignUpStoreBase with _$UserSignUpStore;

abstract class _UserSignUpStoreBase with Store {
  
  @observable
  User loggedUser = User();

  @observable
  String name = '';

  @observable
  String phone = '';

  @observable
  String cep = '';

  @observable
  String email = '';

  @observable
  String password = '';

  @observable
  bool isLoading = false;

  @observable
  bool isLoggedIn = false; 

  @action
  void setName(String value) => name = value;

  @action
  void setPhone(String value) => phone = value;

  @action
  void setCep(String value) => cep = value;

  @action
  void setEmail(String value) => email = value;

  @action
  void setPassword(String value) => password = value;

  @computed
  bool get isNameValid => !isNumeric(name);

  @computed
  bool get isPhoneValid => phone.length > 9;

  @computed
  bool get isCepValid => cep.length > 8;

  @computed
  bool get isEmailValid => isEmail(email);

  @computed
  bool get isPasswordValid => password.length > 5;

  @computed
  Function get firstFormPressed => (isNameValid &&
          isCepValid &&
          isPhoneValid &&
          isEmailValid &&
          isPasswordValid &&
          !isLoading)
      ? signUp
      : null;

  @action
  Future<User> signUp() async {
    isLoading = true;
    User user = User(
      id: null,
      name: name,
      cep: cep,
      phone: phone,
      role: 'ADMIN',
      email: email,
      password: password,
    );
    Response response = await post(
      Api.BASE_URL + 'cliente',
      headers: Api.BASE_HEADER,
      body: jsonEncode(
        user.toJson(),
      ),
    );

    if (response.statusCode == 201) {
      await Future.delayed(Duration(seconds: 2));
      isLoading = false;
      isLoggedIn = true;
      loggedUser = User.fromJson(jsonDecode(response.body));
      return loggedUser;
    }

    return null;
  }

}