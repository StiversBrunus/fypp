import 'package:flutter/material.dart';

class ShowImageScreen extends StatelessWidget {
  final int photId;
  ShowImageScreen({Key key, this.photId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black.withOpacity(0.1),
      appBar: AppBar(
        title: Text('Foto'),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Center(
        child: Image.asset('assets/images/1.jpg'),
      ),
    );
  }
}
