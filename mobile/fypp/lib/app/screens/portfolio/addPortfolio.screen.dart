import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:fypp/app/controllers/portfolio.controller.dart';
import 'package:fypp/app/controllers/signup_controller.dart';
import 'package:fypp/app/model/photographer.dart';
import 'package:fypp/app/model/portfolio.dart';
import 'package:fypp/app/model/portfolioPhoto.dart';

class AddPortfolioScreen extends StatefulWidget {
  final Photographer phot;
  AddPortfolioScreen({Key key, this.phot}) : super(key: key);

  @override
  _AddPortfolioScreenState createState() => _AddPortfolioScreenState();
}

class _AddPortfolioScreenState extends State<AddPortfolioScreen> {
  SignUpController controller = SignUpController();
  PortfolioController portController = PortfolioController();

  TextEditingController nameController = TextEditingController();
  TextEditingController descController = TextEditingController();

  List<File> images = List<File>();
  List<PortfolioPhoto> photos = [];
  String url = '';

  @override
  void initState() {
    super.initState();
  }

  void getImages() async {
    var files = await FilePicker.getMultiFile(type: FileType.image);
    setState(() {
      images = files;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          for (File image in images) {
            url = await controller.sendProfilePic(image);
            PortfolioPhoto photo =
                PortfolioPhoto(name: image.path, desc: 'teste', url: url);
            photos.add(photo);
          }
          Portfolio port = Portfolio(
            name: nameController.text,
            desc: descController.text,
            photId: widget.phot.id,
            photos: photos,
          );
          Portfolio addedPort = await portController.newPortfolio(port);
          if (addedPort != null) {
            widget.phot.portfolios.add(addedPort);
            Photographer photographer = Photographer(
              id: widget.phot.id,
              name: widget.phot.name,
              birthDate: widget.phot.birthDate,
              phone: widget.phot.phone,
              cpf: widget.phot.cpf,
              experience: widget.phot.experience,
              cep: widget.phot.cep,
              categories: widget.phot.categories,
              email: widget.phot.email,
              password: widget.phot.password,
              profilePic: widget.phot.profilePic,
              portfolios: widget.phot.portfolios,
              token: widget.phot.token,
            );
            portController.updateWithPortfolio(photographer);
          }
        },
        child: Icon(
          Icons.save,
        ),
      ),
      appBar: AppBar(
        title: Text('Novo portfólio'),
        centerTitle: true,
        elevation: 0,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            TextField(
              controller: nameController,
              decoration: InputDecoration(labelText: 'Nome do portfólio'),
            ),
            TextField(
              controller: descController,
              decoration: InputDecoration(labelText: 'Descrição do portfólio'),
            ),
            SizedBox(
              height: 44,
              child: RaisedButton(
                onPressed: () {
                  getImages();
                },
                child: Text('Selecionar imagens'),
              ),
            ),
            Container(
              height: 400,
              child: GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                ),
                itemCount: images.length,
                itemBuilder: (context, index) {
                  if (images != null) {
                    return Container(
                      height: 100,
                      width: 100,
                      child: Image.file(
                        images[index],
                        fit: BoxFit.cover,
                      ),
                    );
                  } else {
                    return Center(
                      child: Text('Selecione as imagens'),
                    );
                  }
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
