import 'package:flutter/material.dart';
import 'package:fypp/app/model/photographer.dart';
import 'package:fypp/app/model/portfolio.dart';
import 'package:fypp/app/model/portfolioPhoto.dart';
import 'package:get/get.dart';

class PhotographerProfile extends StatefulWidget {
  final Photographer phot;
  PhotographerProfile({Key key, this.phot}) : super(key: key);

  @override
  _PhotographerProfileState createState() => _PhotographerProfileState();
}

class _PhotographerProfileState extends State<PhotographerProfile> {
  bool isFav = false;
  List<Portfolio> portfolios = [];
  List<PortfolioPhoto> portPhotos = [];
  Map<String, dynamic> photosMap = {};

  @override
  void initState() {
    super.initState();
    if (widget.phot.portfolios != null) {
      setState(() {
        portfolios = widget.phot.portfolios;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          actions: <Widget>[
            IconButton(
              icon: isFav
                  ? Icon(Icons.favorite, color: Colors.redAccent)
                  : Icon(Icons.favorite),
              onPressed: () {
                setState(() {
                  isFav = !isFav;
                });
                // if(isFav){
                //   Get.snackbar('Favoritos', 'Fotógrafo adicionado aos favoritos');
                // }else{
                //   Get.snackbar('Favoritos', 'Fotógrafo removido dos favoritos');
                // }
              },
            ),
          ],
        ),
        body: Container(
          child: Stack(
            children: <Widget>[
              Container(
                width: double.infinity,
                height: 280,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    colorFilter: ColorFilter.mode(
                      Colors.black.withOpacity(0.1),
                      BlendMode.darken,
                    ),
                    image: AssetImage('assets/images/1.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 90,
                    ),
                    Container(
                      height: 50,
                      width: double.infinity,
                      color: Colors.transparent,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text(
                            '57\nfotos',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                          Text(
                            '3\nportfólios',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                          Text(
                            '11\navaliações',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  padding: EdgeInsets.all(16.0),
                  height: 500,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30),
                    ),
                  ),
                  child: SingleChildScrollView(
                    child: Container(
                      width: double.infinity,
                      padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
                      height: MediaQuery.of(context).size.height,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            widget.phot.name,
                            style: TextStyle(
                              fontSize: 22,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            'São Paulo',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            height: 180,
                            child: ListView.builder(
                              itemCount: 5,
                              scrollDirection: Axis.horizontal,
                              physics: BouncingScrollPhysics(),
                              itemBuilder: (context, index) {
                                portPhotos = portfolios[index].photos;
                                return Container(
                                  margin: EdgeInsets.fromLTRB(0, 10, 10, 0),
                                  height: 180,
                                  width: 110,
                                  color: Colors.blue,
                                  child: Image.network(
                                    portPhotos[index].url,
                                    fit: BoxFit.cover,
                                  ),
                                );
                              },
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            width: double.infinity,
                            height: 50,
                            child: TabBar(
                              indicatorColor: Colors.black,
                              isScrollable: false,
                              labelColor: Colors.black,
                              labelStyle: TextStyle(fontSize: 13),
                              unselectedLabelColor: Colors.grey[500],
                              tabs: [
                                Tab(
                                  text: 'Portfolio',
                                ),
                                Tab(
                                  text: 'Pacotes',
                                ),
                                Tab(
                                  text: 'Avaliações',
                                ),
                              ],
                            ),
                          ),
                          Container(
                            height: 300,
                            child: TabBarView(
                              physics: BouncingScrollPhysics(),
                              children: [
                                Container(
                                  child: Column(
                                    children: portfolios.length >= 1
                                        ? portfolios
                                            .map(
                                              (e) => Text(e.name),
                                            )
                                            .toList()
                                        : <Widget>[
                                            Text('Nenhum portfólio'),
                                          ],
                                  ),
                                ),
                                Icon(Icons.directions_transit),
                                Icon(Icons.directions_bike),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
