import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:fypp/app/controllers/category_controller.dart';
import 'package:fypp/app/model/category.dart';
import 'package:fypp/app/model/photographer.dart';
import 'package:fypp/app/screens/book/event.screen.dart';
import 'package:fypp/app/screens/book/pickphot.screen.dart';
import 'package:fypp/app/stores/category/category.store.dart';
import 'package:get/get.dart';

class PickCategoryScreen extends StatefulWidget {
  PickCategoryScreen({Key key}) : super(key: key);

  @override
  _PickCategoryScreenState createState() => _PickCategoryScreenState();
}

class _PickCategoryScreenState extends State<PickCategoryScreen> {
  CategoryStore categoryStore = CategoryStore();
  CategoryController categoryController = CategoryController();
  List<Category> categories = [];

  @override
  void initState() {
    super.initState();
    setState(() {
      categoryStore.getCategories().then((value) => categories = value);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Escolha a categoria'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Observer(builder: (_) {
            if (categoryStore.loadingData) {
              return Center(
                child: CircularProgressIndicator(backgroundColor: Colors.deepPurple,),
              );
            } else {
              return Column(
                children: categoryStore.categories
                    .map(
                      (e) => InkWell(
                        onTap: () async {
                          // List<Photographer> phots = await categoryController.getPhotographersByCategory(e.id);
                          // Get.to(PickPhotScreen(photographers: phots,));
                          Get.to(EventScreen(category: e,));
                        },
                        child: Container(
                          height: 120,
                          width: double.infinity,
                          margin: EdgeInsets.all(8.0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            image: DecorationImage(
                              colorFilter: ColorFilter.mode(
                                Colors.deepPurple.withOpacity(0.4),
                                BlendMode.darken,
                              ),
                              fit: BoxFit.cover,
                              image: NetworkImage(e.image),
                            ),
                          ),
                          child: Container(
                            margin: EdgeInsets.fromLTRB(15, 15, 0, 0),
                            child: Text(
                              e.name,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                    .toList(),
              );
            }
          }),
        ),
      ),
    );
  }
}
