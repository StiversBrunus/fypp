import 'package:date_field/date_field.dart';
import 'package:flutter/material.dart';
import 'package:fypp/app/components/wave.dart';
import 'package:fypp/app/controllers/category_controller.dart';
import 'package:expandable_bottom_sheet/expandable_bottom_sheet.dart';
import 'package:fypp/app/model/category.dart';
import 'package:intl/intl.dart';

class EventScreen extends StatefulWidget {
  final Category category;
  EventScreen({Key key, this.category}) : super(key: key);

  @override
  _EventScreenState createState() => _EventScreenState();
}

class _EventScreenState extends State<EventScreen> {
  CategoryController categoryController = CategoryController();
  DateTime actualDate = DateTime.now();
  TimeOfDay actualTime = TimeOfDay.now();
  DateFormat dateFormat = new DateFormat('dd/MM/yyyy');

  @override
  void initState() {
    super.initState();
  }

  Future<Null> selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      helpText: 'Data do evento',
      locale: Locale('pt', 'BR'),
      initialDate: actualDate,
      firstDate: actualDate,
      lastDate: DateTime(2023),
    );

    if (picked != null && picked != actualDate) {
      setState(() {
        actualDate = picked;
        print('Date selected: ${actualDate.toString()}');
      });
    }
  }

  Future<Null> selectTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: actualTime,
    );

    if (picked != null && picked != actualTime) {
      setState(() {
        actualTime = picked;
        print('Time selected: ${actualTime.toString()}');
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(''),
        centerTitle: true,
        elevation: 0,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: ExpandableBottomSheet(
          background: Container(
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 12.0, top: 30.0),
                  width: double.infinity,
                  height: 130,
                  color: Colors.deepPurple,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Escolha abaixo',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 22,
                        ),
                      ),
                      Text(
                        'a data, horário e local do evento ;)',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 22,
                        ),
                      ),
                    ],
                  ),
                ),
                Wave(
                  height: 60,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    SizedBox(
                      height: 80,
                      width: 150,
                      child: RaisedButton(
                        color: Colors.deepPurple,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.date_range,
                              color: Colors.white,
                            ),
                            Text(
                              'Escolher data',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            )
                          ],
                        ),
                        onPressed: () {
                          selectDate(context);
                        },
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    SizedBox(
                      height: 80,
                      width: 150,
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12),
                        ),
                        color: Colors.deepPurple,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.access_time,
                              color: Colors.white,
                            ),
                            Text(
                              'Escolher horário',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                        onPressed: () {
                          selectTime(context);
                        },
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          persistentHeader: Container(
            decoration: BoxDecoration(
              color: Colors.deepPurple,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(24),
                topRight: Radius.circular(24),
              ),
            ),
            height: 52,
            child: Center(
              child: Text(
                'detalhes do evento',
                style: TextStyle(color: Colors.white, fontSize: 18),
              ),
            ),
          ),
          expandableContent: Container(
            padding: EdgeInsets.all(8.0),
            height: 200,
            color: Colors.deepPurple,
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: 120,
                    width: double.infinity,
                    margin: EdgeInsets.all(8.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      image: DecorationImage(
                        colorFilter: ColorFilter.mode(
                          Colors.deepPurple.withOpacity(0.4),
                          BlendMode.darken,
                        ),
                        fit: BoxFit.cover,
                        image: NetworkImage(widget.category.image),
                      ),
                    ),
                    child: Container(
                      margin: EdgeInsets.fromLTRB(15, 15, 0, 0),
                      child: Text(
                        widget.category.name,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                        ),
                      ),
                    ),
                  ),
                  Text(
                    'data: ${dateFormat.format(actualDate)}',
                    style: TextStyle(color: Colors.white, fontSize: 16),
                  ),
                  Text(
                    'horário: ${actualTime.format(context)}',
                    style: TextStyle(color: Colors.white, fontSize: 16),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
