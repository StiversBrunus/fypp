import 'package:flutter/material.dart';

class BundleScreen extends StatelessWidget {
  const BundleScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Escolha do pacote'),
        centerTitle: true,
        elevation: 0,
      ),
      body: Container(
        child: Center(
          child: Text('Escolha o pacote que melhor te atende'),
        ),
      ),
    );
  }
}
