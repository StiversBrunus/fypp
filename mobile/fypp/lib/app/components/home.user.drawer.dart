import 'package:flutter/material.dart';
import 'package:fypp/app/components/drawer.tile.dart';
import 'package:fypp/app/components/wave.dart';
import 'package:fypp/app/screens/profile/user.profile.dart';
import 'package:get/get.dart';

class HomeUserDrawer extends StatelessWidget {
  final PageController pageController;
  const HomeUserDrawer({Key key, this.pageController}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.all(0.0),
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(10.0),
            height: 200,
            color: Colors.deepPurple,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    CircleAvatar(
                      radius: 50,
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Nome usuário',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                          ),
                        ),
                        Text(
                          'usuário@usuario.com',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    FlatButton(
                      onPressed: () {
                        Get.to(UserProfile());
                      },
                      child: Text(
                        'ver perfil >',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 14,
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          Wave(
            height: 60,
          ),
          DrawerTile(
            controller: pageController,
            icon: Icons.home,
            text: 'home',
            page: 0,
          ),
          DrawerTile(
            controller: pageController,
            icon: Icons.book,
            text: 'contratar',
            page: 1,
          ),
          DrawerTile(
            controller: pageController,
            icon: Icons.favorite,
            text: 'favoritos',
            page: 2,
          ),
          DrawerTile(
            controller: pageController,
            icon: Icons.settings,
            text: 'configurações',
            page: 3,
          ),
          DrawerTile(
            controller: pageController,
            icon: Icons.exit_to_app,
            text: 'sair',
            page: 4,
          ),
        ],
      ),
    );
  }
}
