import 'package:flutter/material.dart';
import 'package:fypp/app/components/home.user.card.dart';
import 'package:fypp/app/components/phot.card.dart';
import 'package:fypp/app/components/wave.dart';
import 'package:fypp/app/controllers/category_controller.dart';
import 'package:fypp/app/controllers/photographer_controller.dart';
import 'package:fypp/app/model/category.dart';
import 'package:fypp/app/model/photographer.dart';
import 'package:fypp/app/stores/category/category.store.dart';
import 'package:fypp/app/stores/phot/phot.store.dart';

class UserHomeTab extends StatefulWidget {
  const UserHomeTab({Key key}) : super(key: key);

  @override
  _UserHomeTabState createState() => _UserHomeTabState();
}

class _UserHomeTabState extends State<UserHomeTab> {
  PhotographerController photController = PhotographerController();
  CategoryController categoryController = CategoryController();
  PhotStore photStore = PhotStore();
  CategoryStore catStore = CategoryStore();

  List<Photographer> phots = [];
  List<Category> categories = [];

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 12.0, top: 30.0),
              height: 130,
              width: double.infinity,
              color: Colors.deepPurple,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Descubra os',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 22,
                    ),
                  ),
                  Text(
                    'melhores fotógrafos',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 22,
                    ),
                  ),
                ],
              ),
            ),
            Wave(height: 70),
            Container(
              height: 190,
              padding: EdgeInsets.all(8.0),
              child: FutureBuilder(
                future: categoryController.getCategories(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    if (snapshot.data != null) {
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            ' Categorias',
                            style: TextStyle(
                              fontSize: 22,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          Container(
                            height: 140,
                            child: ListView.builder(
                              physics: BouncingScrollPhysics(),
                              itemCount: snapshot.data.length,
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (context, index) {
                                return InkWell(
                                  onTap: () async {
                                    // List<Photographer> phots = await categoryController
                                    //     .getPhotographersByCategory(e.id);
                                    // Get.to(PickPhotScreen(
                                    //   photographers: phots,
                                    // ));
                                  },
                                  child: Container(
                                    height: 120,
                                    width: 210,
                                    margin: EdgeInsets.all(8.0),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(12),
                                      image: DecorationImage(
                                        colorFilter: ColorFilter.mode(
                                          Colors.deepPurple.withOpacity(0.4),
                                          BlendMode.darken,
                                        ),
                                        fit: BoxFit.cover,
                                        image: NetworkImage(
                                          snapshot.data[index].image,
                                        ),
                                      ),
                                    ),
                                    child: Container(
                                      margin: EdgeInsets.fromLTRB(15, 15, 0, 0),
                                      child: Text(
                                        snapshot.data[index].name,
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 20,
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                        ],
                      );
                    }
                  } else {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
              ),
            ),
            Divider(),
            Container(
              padding: EdgeInsets.all(8.0),
              child: FutureBuilder<List<Photographer>>(
                future: photController.getPhotographers(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    if (snapshot.data != null) {
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            ' Fotógrafos em alta',
                            style: TextStyle(
                              fontSize: 22,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          Column(
                            children: snapshot.data
                                .map(
                                  (e) => PhotCard(phot: e,)
                                )
                                .toList(),
                          ),
                        ],
                      );
                    }
                  } else {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
