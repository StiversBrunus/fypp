import 'package:flutter/material.dart';
import 'package:fypp/app/model/photographer.dart';
import 'package:fypp/app/model/portfolio.dart';

class PhotPortfolios extends StatefulWidget {
  final Photographer phot;
  PhotPortfolios({Key key, this.phot}) : super(key: key);

  @override
  _PhotPortfoliosState createState() => _PhotPortfoliosState();
}

class _PhotPortfoliosState extends State<PhotPortfolios> {
  List<Portfolio> portfolios = [];

  @override
  void initState() {
    super.initState();
    setState(() {
      portfolios = widget.phot.portfolios;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
            children: portfolios
                .map(
                  (e) => Card(
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Text(e.name),
                          Text(e.desc),
                        ],
                      ),
                    ),
                  ),
                )
                .toList()),
      ),
    );
  }
}
