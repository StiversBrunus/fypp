import 'dart:convert';

import 'package:fypp/app/model/photographer.dart';
import 'package:fypp/app/model/portfolio.dart';
import 'package:fypp/app/utils/api.dart';
import 'package:http/http.dart';

class PortfolioController {
  Future<Portfolio> newPortfolio(Portfolio port) async {
    Response response = await post(
      Api.BASE_URL + 'portfolio',
      headers: Api.BASE_HEADER,
      body: jsonEncode(
        port.toJson(),
      ),
    );

    if (response.statusCode == 200) {
      return Portfolio.fromJson(jsonDecode(response.body));
    }
    return null;
  }

  Future<Photographer> updateWithPortfolio(Photographer photographer) async {
    Response response = await put(
      Api.BASE_URL + 'fotografo/${photographer.id}',
      headers: Api.BASE_HEADER,
      body: jsonEncode(
        photographer.toJson(),
      ),
    );

    if (response.statusCode == 200) {
      return Photographer.fromJson(jsonDecode(response.body));
    }

    return null;
  }
}
