class Event {
  final int id;
  final String date;
  final String time;
  final String comments;
  final String place;
  final int photographerId;
  final int userId;
  final int categoryId;

  Event({
    this.id,
    this.date,
    this.time,
    this.comments,
    this.place,
    this.photographerId,
    this.userId,
    this.categoryId,
  });
}
